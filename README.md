# JutgITB
## Versió: 3

## Descripció:

JutgeITB és un programari dissenyat per resoldre i corregir problemes amb 
una interfície senzilla. Utilitza persistència de dades per a un seguiment
educatiu eficient. Proporciona una experiència d'ús fàcil tant per al 
seguiment educatiu com per a la resolució de problemes. JutgeITB ofereix 
una plataforma completa que permet als alumnes treballar en diferents 
problemes, triant si volen resoldre'ls o no. En alguns casos, passaran al 
següent problema, mentre que en altres, els alumnes tindran accés a un 
menú per decidir què volen treballar. A més, al final de cada problema 
resolt, es mostra un resum dels intents que l'alumne ha realitzat per 
arribar a la solució. Amb una interfície intuïtiva i un enfocament en 
l'experiència de l'usuari, JutgeITB busca proporcionar una experiència 
educativa còmoda tant per als alumnes com per als professors.

## Instal·lació i execució

Per poder utilitzar aquest programari, és imprescindible treballar amb
PostgreSQL, un sistema de gestió de bases de dades relacional altament 
fiable i amplament utilitzat. PostgreSQL proporciona una plataforma robusta 
i escalable per connectar amb bases de dades i emmagatzemar, gestionar i
consultar la informació de manera eficient. És una eina essencial per a
assegurar un funcionament eficaç del software.


https://www.enterprisedb.com/downloads/postgres-postgresql-downloads

Un cop instalat PostgreSQL, seguiu les següents indicacions:

1. Descarregueu el projecte JutgITB
2. Obriu el projecte amb Intellij Idea
3. Copieu la ruta absoluta del fitxer "jutgedb" del projecte fent clic dret i copiant la seva ruta absoluta
4. Connecteu-vos a PostgreSQL i copieu la següent comanda al promt:
 
       =# \i <enganxeu aquí la ruta absoluta del fitxer "jutgitb.sql">
5. Des del navegador del projecte obre el fitxer "main.kt" i executeu-lo

Diego Aretaga Quevedo 
package clases
import JutgeDB
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlin.io.path.Path
import kotlin.io.path.readText

/**
 * Clase que representa la colección de problemas.
 */
class Problemes {

    var problemes = mutableListOf<Problema>()

    /**
     * Inicializa la lista de problemas cargándolos desde un archivo JSON.
     */
    init {
        try {
            val listaDeProblemasJson = Path("./src/data/problemes.json").readText()
            val listaDeProblemas = Json.decodeFromString<List<Problema>>(listaDeProblemasJson)
            for (problemaInfo in listaDeProblemas) {
                JutgeDB().afegirProblema(problemaInfo)
                problemes = JutgeDB().rebreProblemes()

            }
        } catch (e: Exception) {
            println("Se produjo un error al cargar los problemas desde el archivo JSON: ${e.message}")
        }
    }
}
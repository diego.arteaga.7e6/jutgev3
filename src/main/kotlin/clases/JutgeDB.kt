import clases.Problema
import java.sql.SQLException

/**
 * Clase que gestiona la base de datos
 *
 * @author Diego Arteaga
 */
class JutgeDB {

    /**
     * Función que añade un problema a la base de datos.
     *
     * @param problema es el problema que se añade.
     */
    fun afegirProblema(problema: Problema) {
        val query = connection!!.prepareStatement("INSERT INTO Problema (titol, enunciat, entrada, sortida, resolt, NIntets)" +
                "VALUES (?,?,?,?,?,?)")
        try {
            query.setString(1, problema.titol)
            query.setString(2, problema.enunciat)
            query.setString(3, problema.entrada)
            query.setString(4, problema.sortida)
            query.setBoolean(5, problema.resolt)
            query.setInt(6, problema.nIntets)
            query.executeUpdate()
            query.close()
        } catch (e: SQLException) {
            println("Error ${e.errorCode} al afegir el problema: ${e.message}")
        }
    }

    /**
     * Función que recibe una lista de problemas de la base de datos.
     *
     * @return Los problemas recibidos.
     */
    fun rebreProblemes(): MutableList<Problema> {
        val problemes = mutableListOf<Problema>()
        val query = connection!!.createStatement().executeQuery("SELECT * FROM Problema")

        try {
            while (query.next()) {
                val id_problema = query.getInt("id_problema")
                val titol = query.getString("titol")
                val enunciat = query.getString("enunciat")
                val entrada = query.getString("entrada")
                val sortida = query.getString("sortida")
                val problema = Problema(
                    id_problema,
                    titol,
                    enunciat,
                    entrada,
                    sortida
                )
                problema.resolt = query.getBoolean("resolt")
                problema.nIntets = query.getInt("NIntets")
                problema.intents = this.rebreIntents(id_problema)
                problemes.add(problema)
            }
            query.close()
        } catch (e: SQLException) {
            println("Error ${e.errorCode} al rebre els problemes: ${e.message}")
        }

        return problemes
    }

    /**
     * Función que añade un intento a la base de datos.
     *
     * @param idProblema es el id del problema intentado.
     * @param intent el contenido del intento
     */
    fun afegirIntent(idProblema: Int, intent: String) {
        try {
            connection!!.createStatement().executeUpdate("INSERT INTO Intents (problema_id, intent) VALUES ($idProblema, '$intent')")
        } catch (e: SQLException) {
            println("Error ${e.errorCode} al afegir l'intent: ${e.message}")
        }
    }

    /**
     * Función que recibe una lista de intentos de la base de datos.
     *
     * @return Los intentos recibidos.
     */
    fun rebreIntents(idProblema: Int): MutableList<String> {
        val intents = mutableListOf<String>()
        val query = connection!!.createStatement().executeQuery("SELECT * FROM Intents WHERE problema_id = $idProblema")
        try {
            while (query.next()) {
                val intent = query.getString("intent")
                intents.add(intent)
            }
            query.close()
        } catch (e: SQLException) {
            println("Error ${e.errorCode} al rebre els intets: ${e.message}")
        }
        return intents
    }

    /**
     * Función que modifica un problema en la base de datos.
     *
     * @param problema es el problema a modificar.
     */
    fun actualitzarProblema(problema: Problema) {
        val update = "UPDATE Problema SET titol = ?, enunciat = ?, entrada = ?, sortida = ?, resolt = ?, NIntets = ? WHERE id_problema = ?"
        val query = connection!!.prepareStatement(update)
        try {
            query.setString(1, problema.titol)
            query.setString(2, problema.enunciat)
            query.setString(3, problema.entrada)
            query.setString(4, problema.sortida)
            query.setBoolean(5, problema.resolt)
            query.setInt(6, problema.nIntets)
            query.setInt(7, problema.id_problema)
            query.executeUpdate()
            query.close()
        } catch (e: SQLException) {
            println("Error ${e.errorCode} al actualitzar el problema: ${e.message}")
        }
    }
}

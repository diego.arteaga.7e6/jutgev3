package clases
import JutgeDB
import objects.Colors
import alumne
import getId
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import login
import scanner
import java.nio.file.Files
import java.nio.file.StandardOpenOption
import java.text.DecimalFormat
import java.util.*
import kotlin.io.path.Path
import kotlin.system.exitProcess

/**
 * Clase que representa a un profesor del sistema.
 */
class Professor {

    val password = "itb2023"

    /**
     * Permite al profesor iniciar sesión.
     */
    fun loginMestre() {
        var intents = 3
        println("${Colors.light_green}Usuari:${Colors.reset}")
        val userName = scanner.next()
        println("${Colors.light_green}Contrasenya:${Colors.reset}")
        do {
            var exit = false
            try {
                val passwordKey = scanner.next()
                if (passwordKey == password) {
                    println("${Colors.light_green}Benvingut/da $userName. Has iniciat sessió!${Colors.reset}")
                    menuMestre()
                } else if (intents == 0) {
                    println("${Colors.red}Has superat els límit d'intents.${Colors.reset}")
                    login()
                } else {
                    println("${Colors.red}La contrasenya no coincideix.${Colors.reset}")
                    intents--
                    println("Et queden ${intents + 1} intents")
                }
            } catch (e: Exception) {
                println("${Colors.red}Error en la lectura de la contrasenya: ${e.message}.${Colors.reset}")
            }
        } while (!exit)

    }

    /**
     * Muestra el menú principal del profesor.
     */
    fun menuMestre() {
        var opcio: Int
        do {
            try {
                println(
                    "${Colors.light_green}Menu${Colors.reset}\n" +
                            "1 - Afegir problemes\n" +
                            "2 - Treure Report\n" +
                            "0 - Sortir\n" +
                            "Selecciona una opcio:"
                )
                opcio = scanner.nextInt()
                when (opcio) {
                    1 -> afegirProblemes()
                    2 -> treureReport()
                    0 -> login()
                    else -> println("Opcio no valida")
                }
            } catch (e: InputMismatchException) {
                println("Entrada invàlida. Si us plau, introdueix un número enter.")
                scanner.next() // Limpiar el valor no válido del scanner
                opcio = -1 // Opcional: asignar un valor inválido para repetir el bucle
            }
        } while (opcio != 0)
    }

    /**
     * Permite al profesor agregar problemas al sistema.
     */
    fun afegirProblemes() {
        println("Introdueix el titol:")
        var titol = scanner.nextLine()
        println("Introdueix l'enunciat:")
        var enunciat = scanner.nextLine()
        println("Introdueix l'entrada:")
        var entrada = scanner.nextLine()
        println("Introdueix la sortida:")
        var sortida = scanner.nextLine()
        println("Introdueix l'entrada del joc de proves public:")
        var entradaPublica = scanner.nextLine()
        println("Introdueix la sortida del joc de proves public")
        var sortidaPublica = scanner.nextLine()
        println("Introdueix l'entrada del joc de proves privat")
        var entradaPrivada = scanner.nextLine()
        println("Introdueix la sortida del joc de proves privat")
        var sortidaPrivada = scanner.nextLine()

        var opcio = 33
        do {
            try {
                println(
                    "El problema quedaria així:" +
                            "1 - $titol\n" +
                            "2 - $enunciat\n" +
                            "3 - $entrada\n" +
                            "4 - $sortida\n" +
                            "5 - $entradaPublica\n" +
                            "6 - $sortidaPublica\n" +
                            "7 - $entradaPrivada\n" +
                            "8 - $sortidaPrivada\n" +
                            "si vols editar algun camp introdueix el seu numero o introdueix 0 per confirmar i 9 per abortar"
                )


                try {
                    opcio = scanner.nextInt()
                } catch (e: InputMismatchException) {
                    println("Entrada invàlida. Si us plau, introdueix un número enter.")
                    scanner.next() // Limpiar el valor no válido del scanner
                    opcio = -1 // Opcional: asignar un valor inválido para repetir el bucle
                }

                when (opcio) {
                    1 -> titol = scanner.next()
                    2 -> enunciat = scanner.next()
                    3 -> entrada = scanner.next()
                    4 -> sortida = scanner.next()
                    5 -> entradaPublica = scanner.next()
                    6 -> sortidaPublica = scanner.next()
                    7 -> entradaPrivada = scanner.next()
                    8 -> sortidaPrivada = scanner.next()
                    9 -> {
                        println("${Colors.blue}Has sortit.")
                        return
                    }

                    0 -> continue
                    else -> println("Opció no vàlida")
                }
            } catch (e: Exception) {
                println("S'ha produït un error: ${e.message}")
            }
        } while (opcio != 0)


        val id = getId(Problemes())

        try {
            val jocDeProvesPublic = JocDeProves(entradaPublica, sortidaPublica)
            val jocDeProvesPrivat = JocDeProves(entradaPrivada, sortidaPrivada)
            val problemaNou = Problema(id, titol, enunciat, entrada, sortida, jocDeProvesPublic, jocDeProvesPrivat)

            alumne.problemes.add(problemaNou)
            val problemaPerGuardar = Json { prettyPrint = true }.encodeToString(alumne.problemes)

            Files.write(
                Path("./src/data/problemes.json"),
                problemaPerGuardar.toByteArray(),
                StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING
            )

            JutgeDB().afegirProblema(problemaNou)

            println("${Colors.light_green}Problema creat amb exit${Colors.reset}")
        } catch (e: Exception) {
            println("S'ha produït un error en guardar el problema: ${e.message}")
        }
    }

    /**
     * Permite al profesor generar un reporte.
     */
    fun treureReport() {
        var opcio = 0

        do {
            try {
                println(
                    "Menu - Report\n" +
                            "1 - Mostrar puntuacio\n" +
                            "2 - Descomptar per intents\n" +
                            "3 - Mostrar gràfica\n" +
                            "0 - Sortir"
                )
                opcio = scanner.nextInt()

                when (opcio) {
                    1 -> {
                        println("Puntuacio: ${veurePuntuacio()}/10 ")
                    }
                    2 -> {
                        println("Puntuacio: ${descomptarIntents()}/10 ")
                    }
                    3 -> mostrarGrafica()
                    0 -> menuMestre()
                    else -> println("Opcio no valida")
                }
            } catch (e: Exception) {
                println("S'ha produït un error en el menú de Report: ${e.message}")
            }
        } while (opcio != 0)
    }

    /**
     * Muestra la puntuación del alumno.
     *
     * @return La puntuación del alumno.
     */
    fun veurePuntuacio(): Double {
        var problemesResolts = 0.0
        for (p in alumne.problemes) {
            if (p.resolt) problemesResolts++
        }
        var nota = ((problemesResolts * 100) / alumne.problemes.size.toDouble()) * 0.1

        return DecimalFormat("#.##").format(nota).toDouble()
    }

    /**
     * Descontar puntos por intentos adicionales.
     *
     * @return La puntuación actualizada después de descontar los puntos por intentos adicionales.
     */
    fun descomptarIntents(): Double {
        println("Introdueix el percentatge de nota que es resta al exercici per intent adicional ${Colors.gray}(50)${Colors.reset} ")
        val percentatge = scanner.nextInt().toDouble() * 0.01

        var nota = veurePuntuacio()
        var intents = 0.0
        for (p in alumne.problemes) {
            intents += p.nIntets
        }

        intents *= percentatge

        nota -= intents

        return DecimalFormat("#.##").format(nota).toDouble()
    }

    /**
     * Muestra la gráfica con la puntuación y los intentos de cada problema.
     */
    fun mostrarGrafica() {
        println(
            "La nota de l'alumne es ${veurePuntuacio()}/10 punts\n" +
                    "ha realitzat el ${veurePuntuacio()}0% dels problemes\n"
        )
        for (i in alumne.problemes.indices) {
            println("Problema número: ${alumne.problemes[i].id_problema} Te: ${alumne.problemes[i].nIntets} intents ")
        }
    }
}

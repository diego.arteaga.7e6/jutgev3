package clases
import kotlinx.serialization.Serializable

/**
 * Clase que representa un juego de pruebas.
 *
 * @property input Entrada del juego de pruebas.
 * @property output Salida esperada del juego de pruebas.
 */
@Serializable
class JocDeProves(val input: String, val output: String) {
}
package clases
import JutgeDB
import objects.Colors
import kotlinx.serialization.Serializable
import scanner

/**
 * Clase que representa un problema del sistema.
 *
 * @property id_problema El ID del problema.
 * @property titol El título del problema.
 * @property enunciat El enunciado del problema.
 * @property entrada La entrada del problema.
 * @property sortida La salida esperada del problema.
 * @property jocDeProvesPublic Los juegos de prueba públicos asociados al problema.
 * @property jocDeProvesPrivat Los juegos de prueba privados asociados al problema.
 */
@Serializable
data class Problema(
    val id_problema: Int,
    val titol: String,
    val enunciat: String,
    val entrada: String,
    val sortida: String,
    val jocDeProvesPublic: JocDeProves? = null,
    val jocDeProvesPrivat: JocDeProves? = null
) {
    var resolt = false
    var nIntets = 0
    var intents = mutableListOf<String>()

    /**
     * Muestra el problema y sus juegos de prueba.
     */
    fun mostrarProblema() {
        println(
            "${this.enunciat}\n" +
                    "${Colors.recuadro} ${this.jocDeProvesPublic?.input} ${Colors.reset} - " +
                    "${Colors.recuadro} ${this.jocDeProvesPublic?.output} ${Colors.reset}\n" +
                    "${Colors.recuadro} ${this.jocDeProvesPrivat?.input} ${Colors.reset} - " +
                    "${Colors.recuadro} [X] ${Colors.reset}\n  ${Colors.light_lila}Resposta: ${this.jocDeProvesPrivat?.output}${Colors.reset}"
        )
    }

    /**
     * Resuelve el problema.
     */
    fun resoldreProblema() {
        var sortir = false

        do {
            println(
                "${Colors.blue}Vols resoldre'l?: " +
                        "${Colors.green} ■ 1 - si " +
                        "${Colors.yellow} ■ 2 - no " +
                        "${Colors.red} ■ 3 - sortir${Colors.reset}"
            )
            var resposta = scanner.next()
            try {
                val opcio = resposta.toInt()
                when (opcio) {
                    1 -> {
                        println("Inserta la teva resposta:")
                        var respostaUsuari = scanner.next()
                        if (respostaUsuari == this.jocDeProvesPrivat?.output) {
                            println("${Colors.light_cyan}✅ Correcte${Colors.reset}")
                            this.nIntets + 1
                            this.intents.add(respostaUsuari)
                            this.resolt = true
                            resolt = true
                            JutgeDB().afegirIntent(this.id_problema,respostaUsuari)
                            return

                        } else {
                            println("${Colors.red}❌ Error. Proba un altre cop.${Colors.reset}")
                            this.nIntets + 1
                            this.intents.add(respostaUsuari)
                        }
                        JutgeDB().actualitzarProblema(this)
                    }

                    2 -> return
                    3 -> Alumne().menu()
                    else -> {
                        println("Introdueix ${Colors.light_cyan}1${Colors.reset} si vols o ${Colors.yellow}2${Colors.reset} si no en vols resoldre el problema o ${Colors.red}3${Colors.reset} si vols sortir.")
                    }
                }
            } catch (e: NumberFormatException) {
                println("Opció no vàlida. Introdueix un número.")
            }
        } while (!sortir)

    }

    /**
     * Función que recibe una lista de problemas de la base de datos.
     *
     * @param listaDeProblemas La lista de problemas a recibir.
     * @return Los problemas recibidos.
     */
    fun recibirProblemas(listaDeProblemas: List<Problema>): List<Problema> {
        // Realizar operaciones con la lista de problemas recibida
        return listaDeProblemas
    }
}

--Crear la base de datos "JutgeDB"


CREATE DATABASE jutgedb;
-- Crear la tabla "Problema"
CREATE TABLE Problema (
                          id_problema SERIAL PRIMARY KEY,
                          titol TEXT NOT NULL,
                          enunciat TEXT NOT NULL,
                          entrada VARCHAR(33) NOT NULL,
                          sortida VARCHAR(33) NOT NULL,
                          jocdeprovespublic_id INT NOT NULL,
                          jocdeprovesprivat_id INT NOT NULL,
                          resolt BOOLEAN,
                          NIntets INT
);


-- Crear la tabla "JocDeProves"
CREATE TABLE JocDeProves (
                             id_jocdeproves SERIAL PRIMARY KEY,
                             problema_id INT NOT NULL,
                             entrada VARCHAR(33) NOT NULL,
                             sortida VARCHAR(33) NOT NULL,
                             FOREIGN KEY (problema_id) REFERENCES Problema (id_problema)
);


-- Crear la tabla "Intents"
CREATE TABLE Intents (
                         id_intents SERIAL PRIMARY KEY,
                         problema_id INT NOT NULL,
                         intent VARCHAR(33) NOT NULL,
                         FOREIGN KEY (problema_id) REFERENCES Problema (id_problema)
);
